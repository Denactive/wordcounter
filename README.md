# WordCounter

<p align="center">
  <!-- <img alt="" src="https://img.shields.io/badge/LABEL-MSG-0da2ff.svg"> -->
  <a>
    <img alt="Python version" src="https://img.shields.io/badge/Python-3.9-0da2ff.svg">
  </a>
  <a href="https://opensource.org/licenses/LGPL-3.0">
    <img src="https://img.shields.io/badge/License-LGPL%203-0da2ff.svg" alt="License">
  </a>
  <a>
    <img alt="CI" src="https://img.shields.io/badge/GitLabCI-passing-0da2ffe.svg">
  </a>
  <a>
    <img alt="Code Coverage" src="https://img.shields.io/badge/Coverage-100%25-0da2ffe.svg">
  </a>
  <a>
    <img alt="Tests Passed" src="https://img.shields.io/badge/Tests-✔40%20|%20✘0-0da2ffe.svg">
  </a>
</p>

## Description
Multithread text analisys package

The result is a python list of pairs [word, amount] sorted in a lexicographic order

## Usage
- get an array of words. For example, by file reading and calling .split() on its content
- on single thread mode
  - pass the array of words to the **count_words** function ->  you get a dictionary\*
- on multi thread mode\*\*
  - pass the array of words to the **count_words_MT** function -> you get an array of each thread work details\*\*\*
  - merge the **count_words_MT** results by calling **merge** on its return to get the same structure as **count_words** returns

\* the structure looks like:
```python
{
    <a word (str)>: <amount of this word inclusion (int)>
}
```

\*\* it is suggested using amount of threads = cores / 2

\*\*\* the structure looks like:
```python
[{
    'idx': <thread index in a range [0, th_num) (int)>,
    'words': <a list of processed words (list<str>)>,
    'res': {
      <a word (str)>: <amount of this word inclusion (str)>
    }
}]
```

You might use a ready **[main.py](./main.py)**. Pass a file path as an argument, otherwise it runs as a [benchmark](#motivation-for-the-development)


## Motivation for the development
- text analysis basement for use in larger projects
- large texts analysis boost with MT usage

The **benchmark** shows an advantage of MT processing if a text exceeds **180.000** words but still it mostly depends on a threads amount

<table>
  <tr>
    <td>![60.000 words](./benchmark/604x100%20words.png)</td>
    <td>![180.000 words](./benchmark/604x300%20words.png)</td>
    <td>![600.000 words](./benchmark/604x1000%20words.png)</td>
  </tr>
</table>


## Deploy
Just put a [wordcounter](./wordcounter/) folder to your python bins. See `whereis python` or run `pprint(sys.path)` in a python script

## Testing
You might run the tests by input:
```bash
python -m wordcounter.test.test
```
or
```bash
python ./wordcounter/test/test.py
```
or
```bash
python -m unittest discover ./wordcounter/test/
```


**tests output**:

```
test_count_words:    |██████████████████████████████████████████████████| 6 / 6    [ PASSED ]

test_count_words_MT: |██████████████████████████████████████████████████| 16 / 16  [ PASSED ]

test_create_th_data: |██████████████████████████████████████████████████| 7 / 7    [ PASSED ]

test_merge:          |██████████████████████████████████████████████████| 11 / 11  [ PASSED ]
```

You might see the tests coverage by input:
```bash
python -m coverage run -m unittest discover ./wordcounter/test/
```
or
```bash
coverage run -m unittest discover ./wordcounter/test/
```
and see the coverage report:
```bash
coverage report
```

**coverage output**:

```
Name                                     Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------------------------------
wordcounter\src\words_count_sort_MT.py      54      0     24      0   100%
wordcounter\test\test.py                    31      0      4      0   100%
------------------------------------------------------------------------------------
TOTAL                                       85      0     28      0   100%
```

## License
Open-Source | free use | [LGPLv3](https://opensource.org/licenses/LGPL-3.0)
