import unittest
import xmlrunner

import sys
import os
# getting the name of the directory
# where the this file is present.
current = os.path.dirname(os.path.realpath(__file__))
root = os.path.join(current, f'..{os.path.sep}..')
sys.path.append(root)

from wordcounter.src.words_count_sort_MT import count_words, count_words_MT, create_th_data, merge
from wordcounter.test.cases_test_decorator import cases_test
import wordcounter.test.test_cases as test_cases


XML_RESULT_FILE = os.path.join(root, f'report.xml')


class TestWordCount(unittest.TestCase):
  # setUp method is overridden from the parent class TestCase
  def setUp(self):
    pass

  def tearDown(self):
    pass

  # Each test method starts with the keyword test_
  @cases_test(test_cases.merge_cases)
  def test_merge(self, test, answer):
    self.assertDictEqual(merge(*test), answer)

  @cases_test(test_cases.count_words_cases)
  def test_count_words(self, test, answer):
    self.assertListEqual(count_words(test), answer)

  @cases_test(test_cases.create_th_data_cases)
  def test_create_th_data(self, test, answer):
    if test[1] == 0:
      self.assertRaises(ZeroDivisionError, create_th_data, test[0], test[1])
      return
    self.assertListEqual(create_th_data(test[0], test[1]), answer)

  @cases_test(test_cases.count_words_MT_cases)
  def test_count_words_MT(self, test, answer):
    if test[1] == 0:
      self.assertRaises(ZeroDivisionError, count_words_MT, test[0], test[1])
      return
    self.assertListEqual(count_words_MT(test[0], test[1]), answer)

if __name__ == "__main__":
  with open(XML_RESULT_FILE, mode='w', encoding='utf-8') as output:
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output=output))
  
