from pprint import pprint
from wordcounter.test.progress_bar import print_progress_bar


def cases_test(cases, status_offset = 16):
  '''
  @decorator
  --------
  Обертка над методами test_* для класса unittest.TestCase из unittest
  
  - Красивый progress_bar
  - Выравнивание слева по самому длиному названию функции test_*
  - Выравнивание справа зависит от числа кейсов и от того, показывается ли прогресс в %

        cases = [
          {
            'test': any
            'answer': any
          },
          ...
        ]

  - значения из массива cases парой передаются в test_* метод как аргументы test, answer
   - test_* метод имеет следующую сигнатуру: test_*(self, test, answer)

  пример:
  --------

  @cases_test(merge_cases)
  def test_merge(self, test, answer):
    self.assertDictEqual(merge(*test), answer)
  '''

  def gen_test_cases_wrapper(test_func):
    def test(self):
      this_function_name = test_func.__name__
      prefix_margin = max(map(lambda x: len(x), filter(lambda x: x.startswith('test_'), dir(self)))) + 1
      # percent_margin = 6  # 100.0%
      percent_margin = 2 * (len(cases) // 10) + 3  # ii / nn
      

      # фиксированные параметры для progress_bar
      def std_progress_bar_opts(i, suffix, print_end='\r'):
        print_progress_bar(i, len(cases),
            prefix = this_function_name + ':', prefix_margin=prefix_margin,
            suffix=('{:>' + str(status_offset - percent_margin) + '}').format(suffix),
            length = 50, percentage=False, print_end=print_end)
        

      std_progress_bar_opts(0, '')

      for i, case in enumerate(cases):
        std_progress_bar_opts(i, '[ RUNNING ]')

        try:
          test_func(self, case['test'], case['answer'])
        except Exception as e:
          # перехватываю искючение, чтобы сделать вывод на экран не поверх progress_bar
          std_progress_bar_opts(i, '[ FAILED ]')
          print('\n| -> Error on testcase:')
          pprint(case)
          print()
          raise e

      std_progress_bar_opts(len(cases), '[ PASSED ]', print_end='\r\n')
      

    return test
  return gen_test_cases_wrapper
