def print_progress_bar (iteration, total,
    prefix = '', prefix_margin=20,
    suffix = '', suffix_margin=1,
    decimals = 1, length = 100, fill = '█', print_end = "\r",
    percentage=True):
  """
  Call in a loop to create terminal progress bar
  @params:
      iteration     - Required  : current iteration (Int)
      total         - Required  : total iterations (Int)
      prefix        - Optional  : prefix string (Str)
      prefix_margin - Optional  : prefix margin (Int)
      suffix        - Optional  : suffix string (Str)
      suffix_margin - Optional  : suffix margin (Int)
      decimals      - Optional  : positive number of decimals in percent complete (Int)
      length        - Optional  : character length of bar (Int)
      fill          - Optional  : bar fill character (Str)
      print_end     - Optional  : end character (e.g. "\r", "\r\n") (Str)
      percentage    - Optional  : if True progress is shown in percents, i / total otherwise
  """
  percent = ("{0:." + str(decimals) + "f}%").format(100 * (iteration / float(total))) if percentage else str(iteration) + ' / ' + str(total)
  filledLength = int(length * iteration // total)
  bar = fill * filledLength + '-' * (length - filledLength)
  print(('\r{:' + str(prefix_margin) + '} |{}| {} {:>' + str(suffix_margin) + '}').format(prefix, bar, percent, suffix), end = print_end)
  # Print New Line on Complete
  if iteration == total: 
    print()
