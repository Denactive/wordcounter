merge_cases = [
      {
        'test': [
          [('hello', 4)],
          [('hello', 2), ('hello3', 1), ('hello4', 1)],
          [('hello5', 1), ('hello6', 1), ('hello7', 2)],
          [('ahello', 1), ('hello7', 5)],
        ],
        'answer': {
          'ahello': 1,
          'hello': 6,
          'hello3': 1,
          'hello4': 1,
          'hello5': 1,
          'hello6': 1,
          'hello7': 7,
        }
      },
      # немного некорректный тест. Внутри массивов таплы не отсортированы
      {
        'test': [
          [('xahello', 1), ('hello', 6)],
          [('hello3', 1), ('hello4', 1)],
          [('hello3', 15), ('hello6', 1), ('hello7', 7)]
        ],
        'answer': {
          'hello3': 16,
          'hello4': 1,
          'hello6': 1,
          'hello7': 7,
          'xahello': 1,
          'hello': 6,
        }
      },
      # немного некорректный тест. Внутри массивов таплы не отсортированы
      {
        'test': [
          [('xahello', 1), ('hello', 6)],
          [('hello3', 1), ('hello', 1)],
          [('hello3', 15), ('hello6', 1), ('hello7', 7)]
        ],
        'answer': {
          'hello3': 16,
          'hello': 7,
          'hello6': 1,
          'hello7': 7,
          'xahello': 1,
        }
      },
      {
        'test': [
          [('ahello', 1), ('hello', 6)],
          [('ahello', 2), ('hello4', 1)],
          [('hello3', 15), ('hello6', 1), ('hello7', 7)]
        ],
        'answer': {
          'ahello': 3,
          'hello': 6,
          'hello3': 15,
          'hello4': 1,
          'hello6': 1,
          'hello7': 7,
        }
      },
      {
        'test': [[]],
        'answer': {},
      },
      {
        'test': [],
        'answer': {},
      },
      {
        'test': [[],[],[],[]],
        'answer': {},
      },
      {
        'test': [],
        'answer': {},
      },
      {
        'test': [[('hello', 1)]],
        'answer': {'hello': 1},
      },
      {
        'test': [[('hello', 1), ('hello1', 1)]],
        'answer': {'hello': 1, 'hello1': 1},
      },
      {
        'test': [[('hello', 1), ('hello', 1)]],
        'answer': {'hello': 2},
      },
    ]
