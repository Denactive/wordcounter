h = 'hello'
h1 = 'hello1'
h3 = 'hello3'
x = 'x'

count_words_cases = [
  {
    'test': ['hello', 'hello', 'hello', 'hello'],
    'answer': [('hello', 4)]
  },
  {
    'test': ['hello3', 'x', 'hello3', 'hello'],
    'answer': [('hello', 1), (h3, 2), (x, 1)],
  },
  {
    'test': ['hello'],
    'answer': [('hello', 1)],
  },
  {
    'test': ['ab', 'aa', 'ab', 'a'],
    'answer': [('a', 1), ('aa', 1), ('ab', 2)],
  },
  {
    'test': [],
    'answer': [],
  },
  {
    'test': [ '', '', '', ''],
    'answer': [('', 4)],
  },
]
