count_words_MT_cases = [
  {
    'test': [[], 1],
    'answer' : [{
      'idx': 0,
      'res': [],
      'words': [],
    }]
  },
  {
    'test': [[''], 1],
    'answer' : [{
      'idx': 0,
      'res': [('', 1),],
      'words': [''],
    }]
  },
  {
    'test': [['hello'], 1],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 1),],
      'words': ['hello'],
    }]
  },
  {
    'test': [['hello', 'hello'], 1],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 2),],
      'words': ['hello', 'hello'],
    }]
  },
  {
    'test': [['hello', 'hello1'], 1],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 1), ('hello1', 1),],
      'words': ['hello', 'hello1'],
    }]
  },
  {
    'test': [['hello', 'hello1', 'hello'], 1],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 2), ('hello1', 1),],
      'words': ['hello', 'hello1', 'hello'],
    }]
  },
  {
    'test': [['hello'] * 6 + ['hello3', 'hello4', 'hello5', 'hello6'] + ['hello7'] * 7 + ['ahello'], 1],
    'answer' : [{
      'idx': 0,
      'res': [('ahello', 1), ('hello', 6), ('hello3', 1), ('hello4', 1), ('hello5', 1), ('hello6', 1), ('hello7', 7),],
      'words': ['hello'] * 6 + ['hello3', 'hello4', 'hello5', 'hello6'] + ['hello7'] * 7 + ['ahello'],
    }]
  },

# 2 threads | №8

  {
    'test': [[], 2],
    'answer' : [{
      'idx': 0,
      'res': [],
      'words': [],
    },
    {
      'idx': 1,
      'res': [],
      'words': [],
    }]
  },
  {
    'test': [[''], 2],
    'answer' : [{
      'idx': 0,
      'res': [('', 1),],
      'words': [''],
    },
    {
      'idx': 1,
      'res': [],
      'words': [],
    }]
  },
  {
    'test': [['hello'], 2],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 1),],
      'words': ['hello'],
    },
    {
      'idx': 1,
      'res': [],
      'words': [],
    }]
  },
  {
    'test': [['hello', 'hello'], 2],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 1),],
      'words': ['hello'],
    },
    {
      'idx': 1,
      'res': [('hello', 1),],
      'words': ['hello'],
    }]
  },
  {
    'test': [['hello', 'hello1'], 2],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 1),],
      'words': ['hello'],
    },{
      'idx': 1,
      'res': [('hello1', 1),],
      'words': ['hello1'],
    }]
  },
  {
    'test': [['hello', 'hello1', 'hello'], 2],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 1), ('hello1', 1),],
      'words': ['hello', 'hello1'],
    },
    {
      'idx': 1,
      'res': [('hello', 1),],
      'words': ['hello',],
    }]
  },
  {
    'test': [['hello'] * 6 + ['hello3', 'hello4', 'hello5', 'hello6'] + ['hello7'] * 7 + ['ahello'], 2],
    'answer' : [{
      'idx': 0,
      'res': [('hello', 6), ('hello3', 1), ('hello4', 1), ('hello5', 1),],
      'words': ['hello'] * 6 + ['hello3', 'hello4', 'hello5'],
    },
    {
      'idx': 1,
      'res': [('ahello', 1), ('hello6', 1), ('hello7', 7),],
      'words': ['hello6'] + ['hello7'] * 7 + ['ahello'],
    }]
  },
# 0 threads ERROR test | №15
  {
    'test': [[], 0],
    'answer' : []
  },
  {
    'test': [['hello'] * 6, 0],
    'answer' : []
  },
]
