create_th_data_cases = [
  {
    'test': [[], 1],
    'answer': [
      {
        'idx': 0,
        'words': [],
        'res': {}
      }
    ],
  },
  {
    'test': [['any', 'words'], 0],
    'answer': [],
  },
  {
    'test': [[''], 1],
    'answer': [
      {
        'idx': 0,
        'words': [''],
        'res': {}
      }
    ],
  },
  {
    'test': [[''], 2],
    'answer': [
      {
        'idx': 0,
        'words': [''],
        'res': {}
      },
      {
        'idx': 1,
        'words': [],
        'res': {}
      }
    ],
  },
  {
    'test': [['h1', 'h2', 'h3',] * 2, 2],
    'answer': [
      {
        'idx': 0,
        'words': ['h1', 'h2', 'h3'],
        'res': {}
      },
      {
        'idx': 1,
        'words': ['h1', 'h2', 'h3'],
        'res': {}
      }
    ],
  },
  {
    'test': [['h1', 'h2', 'h3'], 2],
    'answer': [
      {
        'idx': 0,
        'words': ['h1', 'h2'],
        'res': {}
      },
      {
        'idx': 1,
        'words': ['h3'],
        'res': {}
      }
    ]
  },
  {
    'test': [['h1', 'h2', 'h3', 'h4'], 3],
    'answer': [
      {
        'idx': 0,
        'words': ['h1', 'h2'],
        'res': {}
      },
      {
        'idx': 1,
        'words': ['h3', 'h4'],
        'res': {}
      },
      {
        'idx': 2,
        'words': [],
        'res': {}
      }
    ]
  },
]
