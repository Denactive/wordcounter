import math
import threading

MERGE_DEBUG = False

def count_words(words):
  # As of Python 3.7 (and CPython 3.6), standard dict is guaranteed to preserve order and is more performant than OrderedDict
  map = dict.fromkeys(words, 0)
  for word in words:
    map[word] = map[word] + 1 
  res = sorted(map.items())
  return res

def count_words_MT(words, th_num):
  def worker(th_data):
    th_data['res'] = count_words(th_data['words'])

  th_data = create_th_data(words, th_num)
  for i in range(th_num):
    t = threading.Thread(target=worker, args=(th_data[i], ))
    t.start()
    t.join()
  
  return th_data

def merge(*arrays):
  arrays = list(filter(lambda array: len(array), arrays))
  positions = [0]  * len(arrays)
  res = {}
  while sum(positions) != -len(arrays):
    column = [arrays[i][pos] for i, pos in enumerate(positions)]
    if MERGE_DEBUG: print('positions:', positions, ' |  column:', column)
    minimum = column[0][0]
    minimum_idxs = []
    minimum_num = 0
    for i, (el, num) in enumerate(column):
      if el < minimum:
        minimum = el
        minimum_num = num
        minimum_idxs = [i]
      elif el == minimum:
        minimum_num = minimum_num + num
        minimum_idxs.append(i)
      
    if MERGE_DEBUG: print(minimum, minimum_num, minimum_idxs)
    res[minimum] = (0 if not minimum in res else res[minimum]) + minimum_num
    for i in minimum_idxs:
      positions[i] = positions[i] + 1
    # удаляю с конца, чтобы не было проблем из-за смещения индексов
    for i in reversed(minimum_idxs):
      if positions[i] == len(arrays[i]):
        positions.pop(i)
        arrays.pop(i)
  return res

def create_th_data(words, th_num):
  offset = 0
  offset_step = math.ceil(len(words) / th_num)
  offset_step = 1 if offset_step == 0 else offset_step
  th_data = []
  for i in range(th_num):
    th_data.append({
      'idx': i,
      'words': words[offset : offset + offset_step if offset + offset_step <= len(words) else len(words)],
      'res': {}
    })
    offset = offset + offset_step
  return th_data
