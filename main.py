import os
import sys
from time import time
from operator import itemgetter
from datetime import datetime
import psutil

import wordcounter as wc


NUM_TH = psutil.cpu_count(True) // 2
BENCHMARK_DEFAULT = True
BENCHMARK_VERBOSE = False
BENCHMARK_ITER_NUM = 10


def get_file_size(filename):
  st = os.stat(filename)
  return st.st_size

def sizeof_fmt(num, suffix="B"):
  for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
    if abs(num) < 1024.0:
      return f"{num:3.1f} {unit}{suffix}"
    num /= 1024.0
  return f"{num:.1f}Yi{suffix}"

def run_benchmark():
  words = '''
    Most buildings of my university are mainly placed in Basmanny District of Moscow. The district has rich and interesting history itself and, thus, nowadays accumulates the architectural heritage of many centuries.
    At first, it was populated by European settlers in the XVII century when Russia was the Muscovite Tsardom and only gained sovereignty. The district called German Quarter was a bridge between Russian and European cultures which fully flourished during Peter the First reign. In the imperial period it was the place where the best masters were (lived) located and were often in Russian aristocrats’ employ including the emperor family.
    Many technical schools also appeared in German quarter not only because many masters lived there, but due to emperor’s family contribution during the age of enlightenment. Therefore, in 1830 by the decree of Maria Feodorovna, the wife of Emperor Paul the First, Moscow Craft School was founded and this date is considered to be the birth of BMSTU.
    The school has been being renamed and expanded and now it is a university. Nowadays it owns 12 buildings in Basmanny district. The main is the Slobodskiy palace which was built in 1753 by Russian chancellor and then owned by the empress Catherine the Great. Later the palace was redesigned to an empire style. Now it is a monument of a cultural heritage.
    My university evolved together with Basmanny district and its different buildings were founded in different times and thus present different architectural styles. Most of the other buildings were founded in the age of the Soviet Union. They are made in the modernist architectural style. The biggest and the newest is the educational-laboratory building. In front of the main entrance of the educational-laboratory building there is a monument to Korolev, the father of the soviet rocket science, who studied at the university.
    There are other monuments in honor of BMSTU students in Basmanny district. The most valuable are dedicated to the heroic deed of the 7th Bauman division of people’s militia. They furiously fought against fascist invaders on the way to Moscow at the beginning of the Great Patriotic War and contributed a lot to the first German defeat on the Soviet land.
    BMSTU is famous, above all, with its prominent graduates. I have already mentioned Korolev. There are also many well-known aircraft engineers Zhukovsky, Tupolev, Suchoy, Lavochkin, avant-guard architects such as Shuhov, etc.
    It is not a coincidence but a consequence of the university status of the leading technical university of Russia, where advanced industrial engineering disciplines such as strength of materials, aerodynamics, thermodynamics have always been taught. Nowadays it is IT: networking, programming and ML. Now I’m studying these subjects to become a high qualified specialist.
    Having studied for three years now I am able to write my own complicated websites, desktop applications and I even have developed a simple processor.
    My university collaborates with many companies, military holdings, research laboratories and commercial industrial corporations. One of them is the VK company, which has opened a tuition department in my university and which I am thankful for studying web technologies.
    Moreover, apart from specialized subjects all students study technical drawing, mathematics, languages and there are always some humanities such as politics, philosophy and history. This results in the fact that students are offered a job while studying.
    Permanently, I am learning artificial intelligence and as many other students I am able to continue studying for three years more in attempt to enroll to well-known Russian scientific community.
    So, I love my university and really hope that school graduates consider enrolling Bauman university as it is a great start for any young engineer.
  '''.split() * 300

  print('\n==========================')
  print('#   Benchmark\n')
  print('*   SingleThread')
  times = []
  for iter in range(1, BENCHMARK_ITER_NUM + 1):
    start = time()
    wc.count_words(words)
    iter_time = time() - start
    times.append(iter_time)
    if BENCHMARK_VERBOSE: print(f'\t[{iter}] {datetime.fromtimestamp(iter_time).microsecond/1000:3.3f} ms')


  print(f'     -> AVG: {datetime.fromtimestamp(sum(times) / BENCHMARK_ITER_NUM).microsecond/1000:3.3f} ms <-')
  print('==========================\n')


  print('*   MultiThread')
  num_th_avg_times = []

  for num_cpu in range(2, psutil.cpu_count(True) + 1):
    times = []
    for iter in range(1, BENCHMARK_ITER_NUM + 1):
      start = time()
      wc.count_words_MT(words, num_cpu)
      iter_time = (time() - start)
      times.append(iter_time)
      if BENCHMARK_VERBOSE: print(f'\t[{iter}] {datetime.fromtimestamp(iter_time).microsecond/1000:3.3f} ms')
    num_th_avg_times.append((num_cpu, sum(times) / BENCHMARK_ITER_NUM))
    print(f'     -> CORES: {num_cpu} | AVG: {datetime.fromtimestamp(sum(times) / BENCHMARK_ITER_NUM).microsecond/1000:3.3f} ms <-')
    if BENCHMARK_VERBOSE: print('==========================\n')
  
  best = min(num_th_avg_times, key=itemgetter(1))
  print(f'    FASTEST: {datetime.fromtimestamp(best[1]).microsecond/1000:3.3f} ms ON {best[0]} CORES')
  print('==========================\n')
   


def main():
  if len(sys.argv) < 2:
    if BENCHMARK_DEFAULT:
      run_benchmark()
    else:
      print('Filename expected')
    return

  file_size = get_file_size(sys.argv[1])
  print(f'program: {sys.argv[0]} | filename: {sys.argv[1]} | size: {sizeof_fmt(file_size, "b")}\n')
  

  with open(sys.argv[1], "r") as f:
    file_content = ''

    try:
      file_content = f.read()
    except OSError as e:
      print('OSError:', e)
      f.close()
      return
      
    words = file_content.split()

    # Async MT
    results = wc.count_words_MT(words, NUM_TH)
    
    print(wc.merge(*map(itemgetter('res'), results)))

    return 


if __name__ == '__main__':
  main()

